## 2.0.0
1. 适配DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK: API11 (4.1.0.36)
2. ArkTs新语法适配
3. retrofit库源码模块重命名为library

## 1.0.3

1. 适配DevEco 3.1.0.100

## 1.0.2

1. 项目适配stage模型，配置文件API升级，修改关键字，demo添加网络权限。

## 1.0.1

1. 适配api9。

## 1.0.0

1. 在Openharmony中移植适配retrofit,当前实现功能：以注解的形式定义接口。 


